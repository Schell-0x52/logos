# Task List

## IN PROGRESS

## TODO

- **0006** - Script for generating doc task file
- **0005** - Organize workspace with a lib, a binary, a game example

## DONE

- **0004** - Find tools for profiling
- **0003** - Find a logger lib
- **0002** - Create cargo project
- **0001** - Create Task list
- **0000** - First README.md
