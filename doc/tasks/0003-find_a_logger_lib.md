
I will probably need an advanced logging system, especially for multithreaded tasks.
`tracing` seems a good candidate. I read the doc and don't see how exactly take benefit from the `span` system. 

I will use `simplelog` for beginning, then switch to something like `tracing` when it becomes required.
