use log::*;
use simplelog::*;

use std::fs::File;

fn main() {
    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Info,
            Config::default(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Info,
            Config::default(),
            File::create("logos.log").unwrap(),
        ),
    ])
    .unwrap();

    info!("Starting Logos.");
}
